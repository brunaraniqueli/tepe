package br.edu.ufsj.dcomp.ges.etss;

import org.apache.poi.ss.usermodel.DataValidationConstraint;

public class Data {

	private int type;
	private int operator;
	private String value1;
	private String value2;

	public Data(DataValidationConstraint dataValidationConstraint) {
		this.type = dataValidationConstraint.getValidationType();
		this.operator = dataValidationConstraint.getOperator();
		this.value1 = dataValidationConstraint.getFormula1();
		this.value2 = dataValidationConstraint.getFormula2();
	}

	public int getType() {
		return type;
	}

	public int getOperator() {
		return operator;
	}

	public String getValue1() {
		return value1;
	}

	public String getValue2() {
		return value2;
	}

}
