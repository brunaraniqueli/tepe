package br.edu.ufsj.dcomp.ges.etss;

import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.DATE;
import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.DECIMAL;
import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.INTEGER;
import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.LIST;
import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.TEXT_LENGTH;
import static org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType.TIME;

import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;

public class FillTable {

	public static void fill(SheetInformation sheetInformation, int times) {
		XSSFSheet sheet = sheetInformation.getSheet();
		List<XSSFDataValidation> dataValidations = sheetInformation.getSheet().getDataValidations();

		List<XSSFTable> tables = sheet.getTables();
		int count = 0;

		for (XSSFTable table : tables) {
			CTTable ctTable = table.getCTTable();
			boolean totalRow = ctTable.getTotalsRowShown();
			if (totalRow) {
				count = fillTableWithTotalRow(sheetInformation, times, sheet, dataValidations, count, table, ctTable);
			} else {
				count = fillTableWithoutTotalRow(sheetInformation, times, sheet, dataValidations, count, table,
						ctTable);
			}

		}
	}

	private static int fillTableWithTotalRow(SheetInformation sheetInformation, int times, XSSFSheet sheet,
			List<XSSFDataValidation> dataValidations, int count, XSSFTable table, CTTable ctTable) {

		long totalRowIndex = table.getEndRowIndex() - ctTable.getTotalsRowCount();
		int i = table.getStartRowIndex();

		boolean shiftRows = true;
		for (; count < times; i++) {
			Row row = sheet.getRow(i);
			if (row == null) {
				row = sheet.createRow(i);
			} else if (shiftRows && (i > totalRowIndex)) {
				sheet.shiftRows(i, i + 1, times);
				row = sheet.createRow(i);
				shiftRows = false;
			}
			count += fillColumns(sheetInformation, table, times, i, row);
		}

		updateTableRefWithTotals(sheet, ctTable, i);
		return count;
	}

	private static void updateTableRefWithTotals(XSSFSheet sheet, CTTable ctTable, int i) {
		CellRangeAddress cellRange = getCellRange(ctTable);
		updateTableRef(ctTable, cellRange.getLastRow() + 1, i + 1);
		Row totalRow = sheet.getRow(i);
		Iterator<Cell> it = totalRow.cellIterator();
		while (it.hasNext()) {
			Cell cell = it.next();
			if (cell.getCellTypeEnum().equals(CellType.FORMULA)) {
				String formula = cell.getCellFormula();
				formula = formula.replaceAll(String.valueOf(cellRange.getLastRow()), String.valueOf(i));
				cell.setCellFormula(formula);
			}
		}
	}

	private static int fillTableWithoutTotalRow(SheetInformation sheetInformation, int times, XSSFSheet sheet,
			List<XSSFDataValidation> dataValidations, int count, XSSFTable table, CTTable ctTable) {
		int i = table.getStartRowIndex();
		for (; count < times; i++) {
			Row row = sheet.getRow(i);
			if (row == null) {
				sheet.shiftRows(i, i + 1, 1);
				row = sheet.createRow(i);
			}
			count += fillColumns(sheetInformation, table, times, i, row);
		}
		CellRangeAddress cellRange = getCellRange(ctTable);
		updateTableRef(ctTable, cellRange.getLastRow()+1, i);
		return count;
	}
	
	private static int fillColumns(SheetInformation sheetInformation, XSSFTable table, int times, int i, Row row) {
		int count = 0;
		XSSFSheet sheet = sheetInformation.getSheet();
		List<XSSFDataValidation> dataValidations = sheetInformation.getSheet().getDataValidations();
		boolean shouldCount = true;
		for (int j = table.getStartColIndex(); j <= table.getEndColIndex(); j++) {
			Cell cell = getCell(row, j);
			if (isFilled(sheet, i, j, cell)) 
				continue;
			
			if (shouldCount) {
				count++;
				shouldCount = false;
			}
			fillCell(sheetInformation, times, dataValidations, i, j, cell);
		}
		return count;
	}

	private static CellRangeAddress getCellRange(CTTable ctTable) {
		String ref = ctTable.getRef();
		CellRangeAddress cellRange = CellRangeAddress.valueOf(ref);
		return cellRange;
	}

	private static boolean isFilled(XSSFSheet sheet, int i, int j, Cell cell) {
		if (cell == null || isUpdateFormula(sheet, i, j, cell) || !isEmpty(cell)) {
			return true;
		}
		return false;
	}

	private static Cell getCell(Row row, int j) {
		Cell cell = row.getCell(j);
		if (cell == null) {
			cell = row.createCell(j);
		}
		if (cell.getCellTypeEnum().equals(CellType.FORMULA)) {
			String formula = cell.getCellFormula();
			cell.setCellFormula(formula);
			cell = null;
		}
		return cell;
	}

	private static boolean isUpdateFormula(XSSFSheet sheet, int i, int j, Cell cell) {
		Row rowAnt = sheet.getRow(i - 1);
		if (rowAnt != null) {
			Cell cellAnt = rowAnt.getCell(j);
			if (cellAnt != null && cellAnt.getCellTypeEnum().equals(CellType.FORMULA)) {
				String formula = cellAnt.getCellFormula();
				if (formula.contains("$")) {
					formula = formula.replaceAll(String.valueOf(i), String.valueOf(i + 1));
					cell.setCellFormula(formula);
				}
				return true;
			}
		}
		return false;
	}

	private static boolean isEmpty(Cell cell) {
		try {
			if (cell.getStringCellValue() != null && !cell.getStringCellValue().equals("")) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private static void updateTableRef(CTTable ctTable, int oldValue, int newValue) {
		String ref = ctTable.getRef().replaceAll(String.valueOf(oldValue), String.valueOf(newValue));
		ctTable.setRef(ref);
	}

	private static void fillCell(SheetInformation sheetInformation, int times, List<XSSFDataValidation> dataValidations,
			int i, int j, Cell cell) {
		Data data = getData(times, dataValidations, i, j);
		if (data != null) {
			fillRandomData(sheetInformation, cell, data);
		}
	}

	private static Data getData(int times, List<XSSFDataValidation> dataValidations, int i, int j) {
		Data data = getTypeFromCell(dataValidations, i, j);
		if (data == null) { // Cell without data validation
			Data dataRowAnt = getTypeFromCell(dataValidations, i - 1, j);
			if (dataRowAnt != null) { // cell with data validation (row ant)
				updateValidationRange(dataValidations, i - 1, j, i + times);
				data = getTypeFromCell(dataValidations, i, j);
			}
		}
		return data;
	}

	private static void updateValidationRange(List<XSSFDataValidation> dataValidations, int row, int col,
			int newLastRow) {
		for (XSSFDataValidation dataValidation : dataValidations) {
			CellRangeAddressList regions = dataValidation.getRegions();
			CellRangeAddress cellRange = getCellRangeByRegions(regions, row, col);
			if (cellRange == null)
				continue;

			cellRange.setLastRow(newLastRow);
		}
	}

	private static Data getTypeFromCell(List<XSSFDataValidation> dataValidations, int row, int col) {
		Data data = null;
		for (XSSFDataValidation dataValidation : dataValidations) {
			CellRangeAddressList regions = dataValidation.getRegions();
			CellRangeAddress cellRange = getCellRangeByRegions(regions, row, col);
			if (cellRange == null)
				continue;
			data = new Data(dataValidation.getValidationConstraint());
		}
		return data;
	}

	private static CellRangeAddress getCellRangeByRegions(CellRangeAddressList regions, int row, int col) {
		for (CellRangeAddress cellRange : regions.getCellRangeAddresses()) {
			if (isInCellRangeAddress(cellRange, row, col)) {
				return cellRange;
			}
		}
		return null;
	}

	private static boolean isInCellRangeAddress(CellRangeAddress cellRange, int row, int col) {
		int firstRow = cellRange.getFirstRow();
		int endRow = cellRange.getLastRow();
		int firstCol = cellRange.getFirstColumn();
		int endCol = cellRange.getLastColumn();
		return row >= firstRow && row <= endRow && col >= firstCol && col <= endCol;
	}

	private static void fillRandomData(SheetInformation sheetInformation, Cell cell, Data data) {

		switch (data.getType()) {
			case TEXT_LENGTH:
				cell.setCellValue(
						RandomDataGenerator.generateRandomString(data.getOperator(), data.getValue1(), data.getValue2()));
				break;
			case LIST:
				cell.setCellValue(RandomDataGenerator.generateRandomListValues(sheetInformation, data.getOperator(),
						data.getValue1(), data.getValue2()));
				break;
			case DATE:
				cell.setCellValue(
						RandomDataGenerator.generateRandomDate(data.getOperator(), data.getValue1(), data.getValue2()));
				break;
			case TIME:
				cell.setCellValue(
						RandomDataGenerator.generateRandomTime(data.getOperator(), data.getValue1(), data.getValue2()));
				break;
			case INTEGER:
				cell.setCellValue(RandomDataGenerator.generateRandomInteger(data.getOperator(), data.getValue1(),
						data.getValue2()));
				break;
			case DECIMAL:
				cell.setCellValue(RandomDataGenerator.generateRandomDecimal(data.getOperator(), data.getValue1(),
						data.getValue2()));
				break;
			default:
				break;
		}
	}

	public static void fillColor(SheetInformation sheetInformation, List<Cell> listCells) {
		XSSFCellStyle cellStyle = sheetInformation.getWorkbook().createCellStyle();
		cellStyle.setFillForegroundColor(IndexedColors.RED.index);
		cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		for(Cell cell: listCells){
			cell.setCellStyle(cellStyle);
		}
	}

}
