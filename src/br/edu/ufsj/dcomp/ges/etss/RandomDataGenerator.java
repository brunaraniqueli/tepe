package br.edu.ufsj.dcomp.ges.etss;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.poi.ss.usermodel.DataValidationConstraint.OperatorType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

public class RandomDataGenerator {

	public static int generateRandomInteger(int operator, String v1, String v2) {

		Random random = new Random();
		int number = 0;

		int min = v1 == null ? 0 : Integer.parseInt(v1);
		int max = v2 == null ? Integer.MAX_VALUE - 1 : Integer.parseInt(v2);

		switch (operator) {
		case OperatorType.BETWEEN:
			number = random.nextInt((max - min) + 1) + min;
			break;
		case OperatorType.NOT_BETWEEN:
			do {
				number = random.nextInt(Integer.MAX_VALUE);
			} while (number > min && number < max);
			break;
		case OperatorType.GREATER_OR_EQUAL:
			number = random.nextInt(Integer.MAX_VALUE - min) + min;
			break;
		case OperatorType.LESS_OR_EQUAL:
			number = random.nextInt(min + 1);
			break;
		case OperatorType.EQUAL:
			number = min;
			break;
		case OperatorType.NOT_EQUAL:
			do {
				number = random.nextInt(Integer.MAX_VALUE);
			} while (number == min);
			break;
		case OperatorType.GREATER_THAN:
			number = random.nextInt((Integer.MAX_VALUE - min)) + min + 1;
			break;
		case OperatorType.LESS_THAN:
			number = random.nextInt(min);
			break;
		}
		return number;
	}

	public static double generateRandomDecimal(int operator, String v1, String v2) {

		Double min = v1 == null ? 0 : Double.parseDouble(v1);
		Double max = v2 == null ? Double.MAX_VALUE - 1 : Double.parseDouble(v2);

		Integer minInt = min == null ? 0 : min.intValue();
		Integer maxInt = max == null ? Integer.MAX_VALUE : max.intValue();

		Double number = 0d;
		switch (operator) {
		case OperatorType.BETWEEN:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number < min || number > max);
			break;
		case OperatorType.NOT_BETWEEN:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number > min && number < max);
			break;
		case OperatorType.GREATER_OR_EQUAL:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number < min);
			break;
		case OperatorType.LESS_OR_EQUAL:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number > min);
			break;
		case OperatorType.EQUAL:
			number = min;
			break;
		case OperatorType.NOT_EQUAL:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number == min);
			break;
		case OperatorType.GREATER_THAN:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number <= min);
			break;
		case OperatorType.LESS_THAN:
			do {
				number = generateRandomDecimal(operator, minInt, maxInt);
			} while (number >= min);
			break;
		}
		return number;
	}

	public static double generateRandomTime(int operator, String v1, String v2) {

		Double min = v1 == null ? 0 : Double.parseDouble(v1);
		Double max = v2 == null ? Double.MAX_VALUE - 1 : Double.parseDouble(v2);

		Random random = new Random();
		Double number = 0d;
		switch (operator) {
		case OperatorType.BETWEEN:
			do {
				number = random.nextDouble();
			} while (number < min || number > max);
			break;
		case OperatorType.NOT_BETWEEN:
			do {
				number = random.nextDouble();
			} while (number > min && number < max);
			break;
		case OperatorType.GREATER_OR_EQUAL:
			do {
				number = random.nextDouble();
			} while (number < min);
			break;
		case OperatorType.LESS_OR_EQUAL:
			do {
				number = random.nextDouble();
			} while (number > min);
			break;
		case OperatorType.EQUAL:
			number = min;
			break;
		case OperatorType.NOT_EQUAL:
			do {
				number = random.nextDouble();
			} while (number == min);
			break;
		case OperatorType.GREATER_THAN:
			do {
				number = random.nextDouble();
			} while (number <= min);
			break;
		case OperatorType.LESS_THAN:
			do {
				number = random.nextDouble();
			} while (number >= min);
			break;
		}

		return number;
	}

	private static Double generateRandomDecimal(int operator, Integer minInt, Integer maxInt) {
		Random random = new Random();
		int multiplyier = generateRandomInteger(operator, String.valueOf(minInt), String.valueOf(maxInt));
		return multiplyier + random.nextDouble();
	}

	public static String generateRandomString(int operator, String v1, String v2) {

		int min = v1 == null ? 1 : Integer.parseInt(v1);
		int max = v2 == null ? Integer.MAX_VALUE - 1 : Integer.parseInt(v2);

		max = generateRandomInteger(operator, v1, v2);

		String value = "";
		switch (operator) {
		case OperatorType.BETWEEN:
			value = generateRandomString(min, max);
			break;
		case OperatorType.NOT_BETWEEN:
			do {
				value = generateRandomString(1, 255);
			} while (value.length() >= min && value.length() <= Integer.parseInt(v2));
			break;
		case OperatorType.GREATER_OR_EQUAL:
			value = generateRandomString(max, 255);
			break;
		case OperatorType.LESS_OR_EQUAL:
			value = generateRandomString(1, min);
			break;
		case OperatorType.EQUAL:
			value = generateRandomString(min, min);
			break;
		case OperatorType.NOT_EQUAL:
			do {
				value = generateRandomString(1, 255);
			} while (value.length() == min);
			break;
		case OperatorType.GREATER_THAN:
			value = generateRandomString(min, 255);
			break;
		case OperatorType.LESS_THAN:
			value = generateRandomString(1, min == 1 ? min : min - 1);
			break;
		}
		return value;
	}

	public static Date generateRandomDate(int operator, String v1, String v2) {

		Integer min = v1 == null ? 1 : Integer.parseInt(v1);
		Integer max = v2 == null ? getMaxExcelDate() : Integer.parseInt(v2);

		Integer aux = 1;
		switch (operator) {
		case OperatorType.NOT_BETWEEN:
			max = getMaxExcelDate();
			operator = OperatorType.LESS_OR_EQUAL;
			aux = generateRandomInteger(operator, String.valueOf(min), String.valueOf(max));

			while (aux >= min && aux <= max) {
				aux = generateRandomInteger(operator, String.valueOf(min), String.valueOf(max));
			}
			break;
		case OperatorType.GREATER_THAN:
			min++;
		case OperatorType.GREATER_OR_EQUAL:
			operator = OperatorType.BETWEEN;
			break;
		default:
			if (v2 == null && operator != OperatorType.LESS_THAN && operator != OperatorType.LESS_OR_EQUAL) {
				operator = OperatorType.LESS_OR_EQUAL;
			}
			break;
		}
		aux = generateRandomInteger(operator, String.valueOf(min), String.valueOf(max));
		if (aux == 0) {
			aux = 1;
		}
		return DateUtil.getJavaDate(aux);
	}

	public static String generateRandomListValues(SheetInformation sheetInformation, int operator, String v1,
			String v2) {
		CellRangeAddress range = CellRangeAddress.valueOf(v1);
		List<String> values = new ArrayList<>();
		Sheet sheet = sheetInformation.getSheet();
		for (int row = range.getFirstRow(); row <= range.getLastRow(); row++) {
			values.add(sheet.getRow(row).getCell(range.getFirstColumn()).getStringCellValue());
		}
		int index = generateRandomInteger(OperatorType.LESS_THAN, String.valueOf(values.size()), null);
		return values.get(index);
	}

	private static Integer getMaxExcelDate() {
		Integer max;
		Calendar cal = Calendar.getInstance();
		cal.set(2050, 11, 31);

		max = (int) DateUtil.getExcelDate(cal.getTime());
		return max;
	}

	private static String generateRandomString(int min, int max) {
		String letters = "abcdefghijklmnopqrstuvxyz";
		Random random = new Random();
		String saveKeys = "";
		int index = -1;
		for (int j = 0; j < max; j++) {
			index = random.nextInt(letters.length());
			saveKeys += letters.charAt(index);

			if (j >= min) {
				int randomNumber = random.nextInt(10);
				if (randomNumber < 5)
					return saveKeys;
			}
		}
		return saveKeys;
	}

}