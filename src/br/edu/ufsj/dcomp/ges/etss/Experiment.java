package br.edu.ufsj.dcomp.ges.etss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Experiment {

	private static final String directory = "/Users/Bruna/workspace/ETSS/sheets/";
	private static final String results = directory + "Results/";

	private static SheetInformation shop = new SheetInformation(directory, "shop", ".xlsx", false);

	private static SheetInformation travelBudget = new SheetInformation(directory, "travel-budget", ".xlsx", false);

	private static SheetInformation activityTracker = new SheetInformation(directory, "activityTracker", ".xlsx",
			true);

	private static SheetInformation salesTracker = new SheetInformation(directory, "salesTracker", ".xlsx", true);

	private static List<SheetInformation> sheets = Arrays.asList(shop, travelBudget, activityTracker, salesTracker);

	public static void main(String args[]) throws IOException {
		for (SheetInformation sheet : sheets) {
			if (sheet.isRun()) {
				generateTestFiles(sheet);
			}
		}
	}

	private static void generateTestFiles(SheetInformation sheetInformation) {
		for (int x = 10; x < 100000; x *= 10) {
			try {
				FileInputStream file = openFile(sheetInformation, x);

				FillTable.fill(sheetInformation, x);

				List<Cell> listCells = Exercise.exercise(sheetInformation, x);

				FillTable.fillColor(sheetInformation, listCells);
				
				closeFiles(sheetInformation, x, file);
			} catch (FileNotFoundException io) {
				System.out.println("File not found:" + sheetInformation.getPath());

			} catch (IOException io) {
				System.out.println("Problem to open/close file:" + sheetInformation.getPath());
			}
		}
	}

	private static FileInputStream openFile(SheetInformation sheetInformation, int x)
			throws FileNotFoundException, IOException {
		FileInputStream file = new FileInputStream(new File(sheetInformation.getPath()));
		// Get the workbook instance for XLS file
		sheetInformation.setWorkbook(new XSSFWorkbook(file));

		// Get first sheet from the workbook
		sheetInformation.setSheet(sheetInformation.getWorkbook().getSheetAt(0));

		System.out.println("Name:" + sheetInformation.getFilename() + "-" + x + sheetInformation.getExtension());
		return file;
	}

	private static void closeFiles(SheetInformation sheetInformation, int x, FileInputStream file)
			throws FileNotFoundException, IOException {
		FileOutputStream fileOut = new FileOutputStream(
				results + sheetInformation.getFilename() + "-" + x + sheetInformation.getExtension());
		sheetInformation.getWorkbook().write(fileOut);
		fileOut.close();
		file.close();
	}
}
