package br.edu.ufsj.dcomp.ges.etss;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SheetInformation {

	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private String directory;
	private String filename;
	private String extension;
	private boolean run;

	public SheetInformation(String directory, String filename, String extension, boolean run) {
		this.directory = directory;
		this.filename = filename;
		this.extension = extension;
		this.run = run;
	}

	public XSSFWorkbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(XSSFWorkbook workbook) {
		this.workbook = workbook;
	}

	public XSSFSheet getSheet() {
		return sheet;
	}

	public void setSheet(XSSFSheet sheet) {
		this.sheet = sheet;
	}

	public String getDirectory() {
		return this.directory;
	}

	public String getFilename() {
		return filename;
	}

	public String getExtension() {
		return extension;
	}

	public boolean isRun() {
		return run;
	}

	public String getPath() {
		return directory + filename + extension;
	}
}
