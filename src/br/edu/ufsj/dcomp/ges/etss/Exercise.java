package br.edu.ufsj.dcomp.ges.etss;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

public class Exercise {

	 static class CellWithTime {
		Cell cell;
		Long time;
		
		CellWithTime(Cell cell, Long time){
			this.cell = cell;
			this.time = time;
		}
	}
	public static List<Cell> exercise(SheetInformation sheetInformation, int times) throws IOException {

		FormulaEvaluator evaluator = sheetInformation.getWorkbook().getCreationHelper().createFormulaEvaluator();

		System.out.println("Avaliando Formulas - Experimento Carga = " + times + "\n" + "\t\t");
		System.out.println("=================================================\n" + "\t\t");

		long startTotalTime = System.currentTimeMillis();
		List<CellWithTime> listCellsWithTime = new ArrayList<>();
		for (Row row : sheetInformation.getSheet()) {
			Iterator<Cell> iterator = row.cellIterator();
			while (iterator.hasNext()) {
				Cell cell = iterator.next();

				if (cell.getCellTypeEnum().equals(CellType.FORMULA)) {
					long startTime = System.currentTimeMillis();
					evaluator.evaluate(cell);
					String formula = cell.getCellFormula();
					cell.setCellFormula(formula);
					long stopTime = System.currentTimeMillis();
					long elapsedTime = stopTime - startTime;
					
					listCellsWithTime.add(new CellWithTime(cell, elapsedTime));
				}
			}
		}
		listCellsWithTime.sort((CellWithTime c1, CellWithTime c2) -> c2.time.compareTo(c1.time)); //Decreasing ordering 
		
		int size = (int) (listCellsWithTime.size() * 0.25); //size 25% of all cells
		listCellsWithTime = listCellsWithTime.subList(0, size); //gets only the 25% cells
		
		List<Cell> listCells = listCellsWithTime.stream()
								.map(c-> c.cell)
								.collect(Collectors.toList());
		/*List<Cell> listCells = new ArrayList<>();
		for(CellWithTime c: listCellsWithTime){
			listCells.add(c.cell);
		}*/
		
		long stopTotalTime = System.currentTimeMillis();
		long elapsedTotalTime = stopTotalTime - startTotalTime;

		System.out.println("=================================================\n" + "\t\t");
		System.out.println("Tempo Total de Execu��o: " + elapsedTotalTime + "\n" + "\t\t");
		System.out.println("=================================================\n");
		return listCells;
	}
}
